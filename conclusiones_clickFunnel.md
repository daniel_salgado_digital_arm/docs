# ClickFunnels CF

Todas las páginas son SPA los pasos y cargar los datos en cookie se hace con js
Tenemos un subdominio. ditalarm.clickfunnel.com 
Todos los gateways creados son tipo CUSTOM

No se puede usar la palabra 'coreg' es un medicamento, por eso usamos en js y en bbdd 'module'

Hasta que la persona no da a Inscribirme o pasa de step clickFunnel no envía nada

## traducción CF - digitalArm

Funnel_name - Gateway

## Repos
Vip para:
- 
        - orgien, tipo navegador, ip
- decidir si se envía pixel a F/G/I
- para gestión de los steps


[clickfunnels](digitalarmteam/clickfunnels.git): js para construir lógica de Gateways

A grandes rasgos hace: cargar datos, codificar datos sensibles. Ver si ha aceptado o no GDPR. Determinar el country. TODO repasar que los js hacen lo que aquí pone

- arquitectura js:
    - json generales
      - cargados en S3 comunes a varios gateways
      - ej: arm_config_buton_pannel
    - 1er js. bottomcode.js
      - rellena datos en cookie y ipapi (traductor ipv4 a ipv6)
    - 2o js
      - country
      - se cargan los prefom
      - lógica de si lanzar pixel

[arm-intelligence
](digitalarmteam/arm-intelligence.git)- endpoint que recibe los datos (jsones) desde CF

## Datos de prueba

nombre: test test
email: test@test.com
teléfono: 910000000

Cualquiera de este nombre o email evita que se envíe a cliente. Se puede probar 1 de ellos (por ej: nombre) poniendo el email de prueba

## Esquema básico

Elementos comunes.

- Start: página de intro, se usa para dar confianza. 
  - Técnicamente VIP para fijar datos (origen, cookies) 
- Formulario: es donde se sacan los 
- Question:
- Thanks Page:

1) Hot
Son todos iguales.
Start->Formulario->Question->Thanks Page

2) Standard
Más variabilidad. 

Start ---> [opcional] PreFormulario (xej: elegir tipo de chocolate, ¿tus hijos influyen en tu compra?)--> Formulario ---> [opcional] Corregistro (hay step con sub preguntas, y steps condicionales a otras respuestas) --->Thank You ---> Banners

## Web Hooks

Endpoints de DigitalARM para los jsones enviados por clickFunnel (en repo arm-intelligence)

## Apartados clickFunnel

__SETTINGS__

Se configura el endpoint que recibirá al json