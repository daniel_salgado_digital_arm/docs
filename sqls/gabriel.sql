/*
 * -- tarea
 */
/* 
 *
 *  Municipio    Leads
Ávila    1858
Burgos    4530
León    5694
Palencia    1958
Salamanca    4184
Segovia    1957
Soria    954
Valladolid    7420
Zamora    2132
 * */
-- Para deal id_deal 405 quieren:
/*
 *  lista fria, 
 * mujeres    DONE
 * de 18 a 80 años  DONE
 *  en Castilla Leon, DONE
 * agrupados por municipio. 
 * Excluendo 
 * 		Tanto Deduplicados como Finales para deal 450 por 360 días 
 * 		finales para grupo clientes Research por 180 días
 * 
 * ------------------------
 * No existen datos con menores de 18 años
 * */
-- solucion Gabi
SELECT
	zCode.province_name,
	count(distinct(leads.phone))
FROM arm_leads_209 as leads
INNER JOIN arm_zipcodes_es as zCode on zCode.postal_code = leads.zip_code
LEFT JOIN (
	SELECT
		leads.phone
	FROM arm_leads_209 as leads
	INNER JOIN arm_submission_delivery as sub on sub.id_arm = leads.id_arm
	INNER JOIN arm_delivery as delivery on delivery.id_delivery = sub.id_delivery
	WHERE delivery.id_deal = 405
	AND delivery.status = 'Sent'
	AND delivery.is_invoiceable in (0, 1)
	AND DATE(delivery.delivery_at) BETWEEN date_add(curdate(), INTERVAL -360 DAY) AND date_add(curdate(), INTERVAL -1 DAY)
	group by leads.phone
) as exclusionDeal on exclusionDeal.phone = leads.phone
LEFT JOIN (
	SELECT
		leads.phone
	FROM arm_leads_209 as leads
	INNER JOIN arm_submission_delivery as sub on sub.id_arm = leads.id_arm
	INNER JOIN arm_delivery as delivery on delivery.id_delivery = sub.id_delivery
	INNER JOIN arm_deals as deals on deals.id_deal = delivery.id_deal
	INNER JOIN arm_end_clients as endC on endC.id_end_client = deals.id_end_client
	WHERE endC.id_group = 28
	AND delivery.status = 'Sent'
	AND delivery.is_invoiceable in (1)
	AND DATE(delivery.delivery_at) BETWEEN date_add(curdate(), INTERVAL -180 DAY) AND date_add(curdate(), INTERVAL -1 DAY)
	group by leads.phone
) as exclusionGroup on exclusionGroup.phone = leads.phone
WHERE TIMESTAMPDIFF(YEAR, leads.birth_date, CURDATE()) between 18 and 80
AND zCode.id_state = 7
AND leads.gender = 'M'
and date(created_at) < date_add(curdate(), interval -30 day) -- lista fria
AND leads.unsuscribed_at is null
and exclusionDeal.phone is null
and exclusionGroup.phone is null
group by zCode.province_name
order by leads.created_at ASC


-- Mi solución 

select 
	zipcodes.province_name,
	count(*)
	from arm_leads_209 leads
	inner join arm_zipcodes_es zipcodes on zipcodes.postal_code  = leads.zip_code 
	inner join arm_leads_209 leads2 on leads2.id_arm = leads.id_arm
where DATE(leads.birth_date) < DATE_SUB(CURRENT_DATE(), INTERVAL 18 YEAR ) 
AND DATE(leads.birth_date) > DATE_SUB(CURRENT_DATE(), INTERVAL 80 YEAR )
-- Exclusiones
and leads.id_arm NOT IN (
		SELECT submission_delivery.id_arm FROM arm_submission_delivery submission_delivery where id_delivery IN (
		select id_delivery from arm_delivery ad where id_deal = 405 and is_invoiceable = 0 and DATE(delivery_at) > DATE_SUB(CURRENT_DATE(), INTERVAL 360 DAY)
	)
)
and leads.id_arm NOT IN (
		SELECT submission_delivery.id_arm FROM arm_submission_delivery submission_delivery where id_delivery IN (
		select id_delivery from arm_delivery ad where id_deal = 405 and is_invoiceable = 1 and DATE(delivery_at) > DATE_SUB(CURRENT_DATE(), INTERVAL 360 DAY)
	)
)
and leads.id_arm NOT IN (
	select id_arm from arm_leads_209 al where id_arm IN (
		select id_arm from arm_submission_delivery asd where id_delivery In (
			select id_delivery from arm_delivery ad2 where id_deal IN (
				select id_deal from arm_deals ad where id_end_client IN (
					select id_end_client from arm_end_clients aec 
					inner join arm_group_end_clients agec2 on agec2.id_group_end_clients  = aec.id_group
					where agec2.name_group = 'Research'
				)
			)
			and DATE(ad2.delivery_at) > DATE_SUB(CURRENT_DATE(), INTERVAL 180 DAY )
		)
	)
)
and zipcodes.state_name = 'Castilla - Leon'
and leads.gender = 'F' 
and leads.inserted_at < DATE_SUB(CURRENT_DATE(), INTERVAL 30 DAY) -- lista fria
and leads.unsuscribed_at IS NULL  -- ¡¡REGLA DE ORO!! always debe existir
GROUP BY zipcodes.province_code
order by leads.id_arm asc -- ¡¡REGLA DE ORO!!
;

  

/*
 * -- tarea
 */
-- cantiadad de leads que podemos enviar a Ibredrola/Sponsor que tengan
-- contador Iberdrola-PT/ Sponsor replico Deduplicar
-- 1) Replicar en SQL el filtro de Deduplicar 
-- 2) en su deal repliques y agrupes por distrito PORTUGAL,
-- varios codigos postales. CP4 localiza un trozo CP3 localiza un torozo de calle. CP7 = CP4 - CP3. 
-- Identificar como hacemos el cruce zip_pt_new arm_zipcode_cpt_new. El cp3 nos da igual. Unir por cp4 (NO)
-- El resultado que me tiene que salir es 113 (calculado con backoffice por Luis (yo no tengo acceso) editando el Producto #1 dando a Calculate


-- Deal Iberdrola-PT/ Sponsor es 390

SELECT 
	 count(distinct leads.phone)
	-- *
	from arm_leads_177 leads
	inner join arm_getways AS getway ON getway.id_getway = leads.id_getway  
	INNER JOIN arm_deal_getway AS deal_getway ON deal_getway.id_getway = leads.id_getway AND deal_getway.b_enabled = 1
	LEFT JOIN (
		--    Exclude by Clients: 7 days. ( Deduplicate and Final )
		--        Master Distância LDA
		--        Einstein Training LDA
		select phone from arm_leads_177 leads
			inner join arm_submission_delivery submission on submission.id_arm = leads.id_arm
			inner join arm_delivery delivery on delivery.id_delivery = submission.id_delivery 
			inner join arm_deals deals on deals.id_deal = delivery.id_deal 
			inner join arm_clients clients on clients.id_client = deals.id_client 
		-- where leads.unsuscribed_at is null -- regla de oro
		and clients.id_client IN (98, 87) -- -- 75, 87 'Master Distancia SA', 'Master Distância LDA' // select id_client from arm_clients client where client.client_name = 'Master Distancia SA' or client.client_name = 'Master Distância LDA';  
		AND DATE(delivery.delivery_at) >= date_add(curdate(), INTERVAL -7 DAY)
		AND delivery.is_invoiceable IN (0,1) -- final y deduplicate
		group by leads.phone
	) excludedClient on excludedClient.phone = leads.phone
	LEFT JOIN (
		   -- 
		   -- Exclude by End Clients: 180 days. ( Final )
		   --   Iberdrola PT
			select phone from arm_leads_177 leads
			inner join arm_submission_delivery submission on submission.id_arm = leads.id_arm
			inner join arm_delivery delivery on delivery.id_delivery = submission.id_delivery 
			inner join arm_deals deals on deals.id_deal = delivery.id_deal 
			inner join arm_end_clients end_clients on end_clients.id_end_client = deals.id_end_client 
		-- where leads.unsuscribed_at is null -- regla de oro
		where end_clients.id_end_client IN (33) -- 33 = 'Iberdrola PT' // select * from arm_end_clients end_client where end_client.description = 'Iberdrola PT';  -- 33
		AND DATE(delivery.delivery_at) >= date_add(curdate(), INTERVAL -180 DAY)
		AND delivery.is_invoiceable IN (1) -- final
		group by leads.phone
	) excludedEndClient on excludedEndClient.phone = leads.phone
	LEFT JOIN ( 
		--    Exclude by Deal: 8 days. ( Deduplicate and Final )
		--        INP - InterPass - Sponsor
		--        IPT - Iberdrola PT - Sponsor - Phone
		select phone from arm_leads_177 leads
			inner join arm_submission_delivery submission on submission.id_arm = leads.id_arm
			inner join arm_delivery delivery on delivery.id_delivery = submission.id_delivery 
			inner join arm_deals deals on deals.id_deal = delivery.id_deal 
		-- where leads.unsuscribed_at is null -- regla de oro
		where DATE(delivery.delivery_at) >= date_add(curdate(), INTERVAL -8 DAY)
		AND delivery.is_invoiceable IN (1,0) -- final y deduplicate
		AND deals.id_deal IN (80,390)  -- 80,390 // SELECT * from arm_deals deals where deals.description = 'INP - InterPass - Sponsor' or deals.description = 'IPT - Iberdrola PT - Sponsor - Phone'
		group by leads.phone
	) excludeByDeal on excludeByDeal.phone = leads.phone
	LEFT JOIN(
    -- Exclude by End Clients: 180 days. ( Final )
    --    Iberdrola PT
		select phone from arm_leads_177 leads
			inner join arm_submission_delivery submission on submission.id_arm = leads.id_arm
			inner join arm_delivery delivery on delivery.id_delivery = submission.id_delivery 
			inner join arm_deals deals on deals.id_deal = delivery.id_deal 
			inner join arm_end_clients end_clients on end_clients.id_end_client = deals.id_end_client 
			inner join arm_group_end_clients group_end_client on group_end_client.id_group_end_clients  = end_clients.id_group 
		-- where leads.unsuscribed_at is null -- regla de oro
		where DATE(delivery.delivery_at) >= date_add(curdate(), INTERVAL -30 DAY)
		AND delivery.is_invoiceable IN (1) -- final
		AND group_end_client.id_group_end_clients = 3 -- 3 =  'Energy' // select * from arm_group_end_clients agec where name_group = 'Energy';
		group by leads.phone
	) excludedEndGroup on excludedEndGroup.phone = leads.phone
	INNER JOIN arm_deal_getway
	where leads.unsuscribed_at is null -- Regla de oro
	AND deal_getway.id_deal = 390 -- 
	and date(leads.created_at) BETWEEN date_add(curdate(), interval -1 day) AND date_add(curdate(), interval -1 DAY) 
	AND TIMESTAMPDIFF(YEAR, leads.birth_date, CURDATE()) between 30 and 120  --  Age Range: 30 - 120 
	and LEFT(leads.zip_code,1) <> 9  -- Real Zipcode: No Type List: Exclusion    Type of exclusion: Csv    Number of Zipcodes: 1000
	and excludedClient.phone is null
	and excludedEndClient.phone is null
	and excludeByDeal.phone is null
	and excludedEndGroup.phone is null
	-- group by leads.id_getway ,leads.phone
	-- limit 10
;		 

