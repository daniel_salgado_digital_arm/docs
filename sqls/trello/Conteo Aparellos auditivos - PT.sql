/* 
 * https://trello.com/c/hSEFna3T
 * 
 * Hola, porfa podrian enviar conteo:

Códigos postais do concelho de Aveiro 55 años +   // Resultado de backoffice 953 (2022-01-24)

Códigos postais do concelho de Aveiro 65 años +    // Resultado de backoffice 217  (2022-01-24)

**/


select 
	zip_code ,
	leads.*
from arm_leads_177 leads
;


select 
	*
from arm_zipcodes_pt_new 
;


-- query lenta (no la he dejado acabar, más de 90 segundos). Es el left en inner join el que relentiza
explain select
	count(distinct (leads.phone))
	from arm_leads_177 leads
 	inner join arm_zipcodes_pt_new zipCode on zipCode.postal_code = LEFT(leads.zip_code,4) 
	where date(created_at) < date_add(curdate(), interval -30 day)  -- lista fria
	and zipCode.state_name = 'Aveiro' and zipCode.province_name = 'Aveiro'
	and TIMESTAMPDIFF(YEAR, leads.birth_date, CURDATE()) between 55 and 120 -- edad
	and leads.unsuscribed_at IS NULL  -- ¡¡REGLA DE ORO!! always debe existir
;

-- rapido pero da 940 resultados (no 953). 
explain select
	count(distinct (leads.phone))
	from arm_leads_177 leads
 	inner join arm_zipcodes_pt_new zipCode on zipCode.postal_code_7 = leads.zip_code  -- 
	where date(created_at) < date_add(curdate(), interval -30 day)  -- lista fria
	and zipCode.state_name = 'Aveiro' and zipCode.province_name = 'Aveiro'
	and TIMESTAMPDIFF(YEAR, leads.birth_date, CURDATE()) between 55 and 120 -- edad
	and leads.unsuscribed_at IS NULL  -- ¡¡REGLA DE ORO!! always debe existir
;


-- rapido y preciso. El LEFT() en el where
explain select
	count(distinct (leads.phone))
	from arm_leads_177 leads
	where date(created_at) < date_add(curdate(), interval -30 day)  -- lista fria
	and left(leads.zip_code,4)  IN (SELECT DISTINCT postal_code  from arm_zipcodes_pt_new codes where state_name = 'Aveiro' and province_name = 'Aveiro')
	and TIMESTAMPDIFF(YEAR, leads.birth_date, CURDATE()) between 55 and 120 -- edad
	and leads.unsuscribed_at IS NULL  -- ¡¡REGLA DE ORO!! always debe existir
;

