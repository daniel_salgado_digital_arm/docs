/* 
 * https://trello.com/c/7nbsbdXC

 * Hola, porfa enviarme conteo de deal 95 Wizink Atrato con las siguientes caracteristicas:

• A nivel nacional

• Segmentado por código postal: adjunto Excel, mantener formato (si de un CP no hay

registros poner un 0)

• Registros con teléfono móvil

    Edad 30 – 69

    Recencia: del 2016 al 2020

¿Podéis filtrar los registros enviados para WIZINK en los últimos 3 meses, por favor? */

-- 


-- mi version
select 
	zip_code ,
	count(distinct leads.phone)
	from arm_leads_209 leads 
	left join (
		select 
			phone
		from arm_leads_209 leads
			inner join arm_submission_delivery submission on submission.id_arm = leads.id_arm 
			inner join arm_delivery delivery on delivery.id_delivery = submission.id_delivery
			inner join arm_deals deals on deals.id_deal = delivery.id_deal 
			inner join arm_end_clients end_clients on end_clients.id_end_client  = deals.id_end_client 
		-- where leads.unsuscribed_at is null
		where end_clients.id_end_client = 181
		and delivery.is_invoiceable IN (0,1)
		and delivery.status = 'Sent'
		and delivery.delivery_at > date_add(curdate(), INTERVAL -90 DAY )	
	) excludedByZink on excludedByZink.phone = leads.phone
where leads.unsuscribed_at is null
and leads.birth_date BETWEEN date_add(curdate(), INTERVAL -69 YEAR) AND date_add(curdate(), INTERVAL -30 YEAR ) 
and date(leads.created_at) < date_add(curdate(), interval -30 day) -- lista fria
and leads.phone REGEXP '^[76]{1}[0-9]{8}$'
and leads.zip_code IN ( -- rellenar con fichero
)
and excludedByZink.phone is null
group by leads.zip_code
order by leads.zip_code 
;

-- versión Luis

SELECT 
		leads.zip_code,
		COUNT(DISTINCT(leads.phone)) AS leads
	FROM arm_leads_209 AS leads
	LEFT JOIN (SELECT clientLeads.phone
        FROM arm_intelligence.arm_submission_delivery AS clientSub
        INNER JOIN arm_delivery AS clientDelivery ON clientDelivery.id_delivery = clientSub.id_delivery
        INNER JOIN arm_intelligence.arm_leads_209 AS clientLeads ON clientLeads.id_arm = clientSub.id_arm
        INNER JOIN arm_intelligence.arm_deals AS clientDeals ON clientDeals.id_deal = clientSub.id_deal
        WHERE clientDeals.id_end_client = 181	
        	AND clientDelivery.is_invoiceable IN (0,1)
        	AND clientDelivery.status IN ('Sent') 
        	AND DATE(clientSub.created_at) BETWEEN date_add(curdate(), INTERVAL -90 DAY) AND date_add(curdate(), INTERVAL 0 DAY)

	) AS sameCsvClient ON leads.phone = sameCsvClient.phone	
	WHERE
		leads.phone REGEXP '^[76]{1}[0-9]{8}$'
		AND leads.unsuscribed_at IS NULL
		AND sameCsvClient.phone IS NULL -- Exclusion (este caso por cliente final y solo csvs tanto finales como deduplicar)
		AND DATE(created_at) < date_add(curdate(), INTERVAL -30 DAY) -- Lista fria
		AND TIMESTAMPDIFF(YEAR,leads.birth_date,CURDATE()) BETWEEN 30 AND 69
		AND zip_code IN ( -- rellenar con fichero
		)
	GROUP BY
		zip_code
	ORDER BY
		leads.zip_code ASC
