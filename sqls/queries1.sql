/*
 * https://trello.com/c/HYnwY3Ji
 * Hola Chicos,
Necesito que me saquéis los siguientes campos de los teléfonos de abajo:
Edad,Genero,Escolaridad,Nacionalidad,Ciudad​
642725056
642099953
605131422
667274841
621011144
Gracias!!
Oscar Horcajada
Business Develop */

SELECT
leads.phone, 
timestampdiff( YEAR , arm_birthdate  , curdate()) edad, 
-- leads.first_name ,
-- leads.full_name ,
leads.gender ,
-- leads.gender_pixel ,
-- leads.arm_standard_education ,
-- leads.arm_standard_nationality ,
-- leads.nationality_pixel ,
-- source.arm_nationality ,
-- source.arm_standard_nationality ,
-- leads.state, 
-- leads.city, 
( SELECT MAX(ate.arm_description_education_es )
            FROM arm_intelligence.arm_translation_ids_education AS ate
            WHERE (source.arm_education_campus = ate.arm_education_campus) OR (source.arm_education = ate.arm_education)
            	OR (source.arm_standard_education = ate.arm_education_campus) OR (source.arm_standard_education = ate.arm_education)) as education,
(
SELECT
    	arm_country.short_name
    	FROM arm_translation_ids_country
    	inner join arm_country on arm_translation_ids_country.id_country  = arm_country.id_country 
    	WHERE (arm_translation_ids_country.id_country = source.arm_standard_nationality AND source.arm_standard_nationality IS NOT NULL) OR (arm_nationality = source.arm_nationality)
) as nacionality
FROM arm_leads_209 leads
inner join arm_source_209 source on source.id_arm = leads.id_arm
-- inner join arm_country country on country.id_country  = nacionality.arm_nacionality
where leads.phone in (
'642725056',
'642099953',
'605131422',
'667274841',
'621011144'
)
;

-- query hecha 2022-01-28 en la presentación de Grabriel sobre píxeles
-- para ver para entender la alarma del slack en channel 'lead-gen-alerts'
-- FBK - 3 Media Conversions were detected in costs but no conversions found on Database for the Gateway!
select 
-- distinct() ,
leads.utm_term,
 count(1)
-- leads.*
 from arm_leads_177 leads where id_getway = '9289925' 
and date(leads.inserted_at) BETWEEN '2022-01-26' and '2022-01-20' and utm_source = 'FBK'
group by leads.utm_term
;

select * from arm_leads_1


select * from arm_media_costs media_costs where media_costs.id_campaign = '23847966061300005'
 and date(media_costs.created_at) = '2022-01-25'
;
select * from arm_l