-- ---------------------
-- clientes de portugal sin contactos
-- -------------------

select * from arm_contact_book acb ; -- NO se usa arm_contacts

select * from arm_clients clients 
	left join arm_contact_book contact on contact.id_client = clients.id_client 
	where clients.id_country_adm = '177' -- no es correcto
	and contact is null
;

select contact.* from arm_deals deals
	inner join arm_clients clients on clients.id_client = deals.id_client
	left join arm_contact_book contact on contact.id_client = deals.id_client 
	where deals.id_country = '177'
	and contact.id_client is null
limit 10;





-- SELECT count(unique(*)) from arm_leads_209 where id_getway =  9316588


-- TODO repasar si cierto: numero de leads ok y error de un deal y un gateway para 1 día
select 
 case id_rtraslate when 1 then 'ok' when 2 then 'repe' end as  result,
count(id_rtraslate) conteo
from arm_submissions submision
inner join arm_leads_209 leads on leads.id_arm  = submision .id_arm
where submision.id_deal = 50
and leads.id_getway = 9316588
and DATE(submision.delivery_date) = '2022-01-18'
group by id_rtraslate




-- esto estaba mal diseñado en BBDD, arm_end_clients NO debe almacenar el string del grupo, para eso está arm_group_end_clients
select 
 DISTINCT(`group` )
from arm_end_clients aec 


-- Preventiva - Corregistros

select * from arm_submissions submission
where submission.id_rtraslate = 1
and submission.id_deal = 51
order by submission.id_submission desc
limit 100;


-- Preventiva - Corregistros TODOS los datos

select 
	as2.arm_referrer, 
	as2.*  
from arm_source_209 as2;
where as2.arm_referrer is not null; 

select arm_leads_209.id_getway from arm_leads_209 al; 

-- 

select * from arm_getways ag ;

select 
	submission.id_submission ,
	submission.id_arm,
	submission.id_rtraslate,
	submission.response ,
	leads_espanya.utm_source ,
	submission.delivery_date ,
	gate.description
--	submission.*
from arm_submissions submission
inner join arm_leads_209 leads_espanya on leads_espanya.id_arm = submission.id_arm 
inner join arm_getways gate on gate.id_getway = leads_espanya.id_getway 
where submission.id_rtraslate IN  (1)
and submission.id_deal = 51
order by submission.id_submission desc
limit 100;



select al.id_getway from arm_leads_209 al;


-- select via ajax de 

select 
`id_submission` as `idSubmission`, 
`arm_submissions`.`id_arm` as `idArm`, 
`id_rtraslate` as `idRtraslate`, 
`response`,
`delivery_date`,
 `arm_getways`.`description` as `gateDescription` ,
CASE 
	WHEN arm_leads_209.id_arm IS NOT NULL THEN arm_leads_209.utm_source
	WHEN arm_leads_short_209.id_arm IS NOT NULL THEN arm_leads_short_209.utm_source 
ELSE "" END AS utmSource 
	from `arm_submissions` 
	inner join `arm_deals` on `arm_deals`.`id_deal` = `arm_submissions`.`id_deal`
	left join `arm_leads_209` on `arm_leads_209`.`id_arm` = `arm_submissions`.`id_arm` 
	left join `arm_leads_short_209` on `arm_leads_short_209`.`id_arm` = `arm_submissions`.`id_arm`
	inner join `arm_getways`  on `arm_getways`.`id_getway` = `arm_leads_209`.`id_getway`  -- Este fue el cruce que daba error. Tenía que ponerlo debajo de haberlo usado en el left join
where `arm_submissions`.`id_deal` = 232 and `id_process` = 1 order by `delivery_date` desc limit 100



-- contactos asociados al cliente
select 
	 count(*) -- 444
	-- * 
from arm_contact_book acb 



-- ver qué  tiene module_15. Que s han usado la pregunta de repsol
select 
	DISTINCT (arm_getways.description )
--	arm_getways.url ,
--	source.arm_step ,
--	source.name ,
--	source.*
	from arm_source_209 source 
	inner join arm_getways ag on arm_getways.id_getway  = source.funnel_id
	where module_15_q_1 is not null  -- pregunta rellena
	and id_country = 209  -- España
	and arm_getways.b_enabled = 1 -- gateway enabled
	group by arm_getways.description 
	-- where arm_step = 'coreg_simp_repsol'
	-- limit 10
;

SELECT  * from arm_getways ag 
where arm_getways.b_enabled 
;
	
select * from arm_getways ag where id_getway = 4847054
;


select phone  * from arm_leads_209 leads
where leads.id_lead  = 1
;

select id_end_client from arm_end_clients aec where id_group IN (Select id_group_end_clients from arm_group_end_clients agec where agec.name_group = 'Research')
;


select 
count(*) from arm_leads_209 al where id_arm IN (
	select id_arm from arm_submission_delivery asd where id_delivery In (
		select id_delivery from arm_delivery ad2 where id_deal IN (
			select id_deal from arm_deals ad where id_end_client IN (
				select id_end_client from arm_end_clients aec 
				inner join arm_group_end_clients agec2 on agec2.id_group_end_clients  = aec.id_group
				where agec2.name_group = 'Research'
			)
		)
	)
)


select count(*) from arm_leads_209 leads
inner join arm_submission_delivery submission on submission.id_arm = leads.id_arm
inner join arm_delivery delivery on delivery.id_deal = submission.id_deal
inner join arm_deals deals on deals.id_deal = delivery.id_deal
inner join arm_end_clients clients on clients.id_end_client = deals.id_end_client 
-- inner join arm_group_end_clients group_end_clients on group_end_clients.id_group_end_clients = clients.id_end_client 
-- where group_end_clients.name_group = 'Research'
;


-- Excluir los leads enviados a grupo Research
select * from arm_submission_delivery submision 
	inner join arm_leads_209 leads on leads.id_arm = submision.id_arm
	inner join arm_deals deals on deals.id_client = leads.id_c
limit 10
;

-- sacar los id_end_client del grupo Research
select * from arm_leads_209 al where id_arm IN (
	select id_arm from arm_submission_delivery asd where id_delivery In (
		select id_delivery from arm_delivery ad2 where id_deal IN (
			select id_deal from arm_deals ad where id_end_client IN (
				select 
				id_end_client
				from arm_group_end_clients group_end_client 
				inner join arm_end_clients end_client on end_client.id_group = group_end_client.id_group_end_clients 
				and group_end_client.name_group = 'Research' 
			)
		)
	)
)
;

/*
-- All active getways for the deal 390
*/

-- NO FUNCIONA (mirar mas abajo la que está ok)
-- esta relación la hice antes de darme cuenta que xisitía arm_deal_getway. Salen casi los mismos reulstados
-- explain 
select 
	-- count(distinct(gateway.id_getway))
	distinct(gateway.url)
	from arm_getways gateway
	inner join arm_leads_177 leads on leads.id_getway = gateway.id_getway
	inner join arm_submission_delivery submission on submission.id_arm = leads.id_arm 
	inner join arm_delivery delivery on delivery.id_delivery = submission.id_delivery 
	inner join arm_deals deals on deals.id_deal = delivery.id_deal 
where 1=1
	-- and gateway.b_enabled = 1
	and deals.id_deal = 390
; -- 12


-- Sí funciona, comparada con 'Gateways for this deal' en Backoffice http://backoffice.digitalarmagency.com/deals/edit/390
select 
	getway.url,
	getway.description 
from arm_deal_getway deal_getway 
inner join arm_getways getway on getway.id_getway  = deal_getway.id_getway 
where id_deal = 390
and deal_getway.b_enabled = 1
; -- 14


-- Luis
-- "es para ver de un vistazo rápido los leads enviados hoy de api, su cap, leads oks, duplicados, etc"
 SELECT * FROM arm_view_delivery_status_api;
 

-- Ver productos (filtros)  de clientes

SELECT arm_deals.id_deal, sum(filters) FROM arm_products_filters 
right join arm_deals ON arm_deals.id_deal = arm_products_filters.id_deal
group by arm_deals.id_deal 
;

SELECT * from  arm_deals where id_deal = 26;
select * from arm_products_filters where id_deal = 26;
update arm_products_filters set filters='[{"filter":"country","type":"table","value":"177"},{"filter":"product","type":"lead","value":"phone"},{"filter":"inclusion","type":"lead","value":"inclusion"},{"filter":"csvfilter","type":"lead","value":"csv"},{"filter":"cpcsv","type":"lead","value":{"cepes":["2001","2002"],"control":"true"}},{"filter":"idDeal","type":"deal","value":"26"}]'  where id_deal = 26;
select filters  from arm_products_filters where id_deal = 26;


select 
    arm_services.service_name ,
    concat('http://dev-backoffice.digitalarmagency.com/deals/edit/',arm_deals.id_deal ),
	arm_deals.*
from arm_deals
inner join arm_services on arm_deals.id_service  = arm_services.id_service
where arm_services.service_name IN ('TXT', 'CSV', 'EXCEL')
;

select * from arm_deals 
inner join arm_services on arm_deals.id_service  = arm_services.id_service
where arm_services.service_name IN ('CSV')
and steps_fields is null
;

SELECT 
arm_services.service_name,
concat('http://dev-backoffice.digitalarmagency.com/deals/edit/',arm_deals.id_deal )
from arm_deals 
inner join arm_services on arm_deals.id_service  = arm_services.id_service
where 1=1
and arm_services.service_name IN ('CSV', 'EXCEL', 'TXT')
and steps_fields is null;
;

-- ver donde que las peticiones de 'duplicated' (desde clckFunnel) están llegando bien
-- campos arm_fire_pixel_(fbk|ggl|outbrain|cft|taboola|twitter)
SELECT * from arm_source_209 as2 limit 1;


select * from arm_deal_types adt ;

select * from arm_deals where id_service = 8;

select * from arm_services;


