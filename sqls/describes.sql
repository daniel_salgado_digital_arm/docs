SHOW PROCESSLIST;


/*
 * Buscar COLUMN_NAME
 */
SELECT DISTINCT TABLE_NAME, COLUMN_NAME 
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE COLUMN_NAME LIKE '%country%'
        AND TABLE_SCHEMA='arm_intelligence'
;


/* Tamaños bbdd */
SELECT table_schema AS "Base de datos",
ROUND(SUM(data_length + index_length) / 1024 / 1024, 2) AS "Tamaño (MB)",

ROUND(SUM(data_length + index_length) / 1024 / 1024 / 1024, 2) AS "Tamaño (GB)"
FROM information_schema.TABLES
GROUP BY table_schema;
       
       
/**
 * Sitio dnd poner las tablas más importantes
 * */
       
-- columnas false-friends
id_country
id_state

-- columnas vip
arm_lead_{id_country}.unsubscribed_at. Regla de ORO. Si envias alguno que este unsubscribed_at, es motivo de despido inmediato y juicio
getway.b_enabled

       
       

-- select * from arm_country ac

-- tabla que almacena el BRUTO de los leads. Por cada cambio de step en clickFunnels llegan 2 json (uno mini otro con más detalle) todos con mismo id_arm
select
    *
    -- count(*)
from arm_sources_json asj limit 1;

-- tabla en la que se segrega por pais (209 España, 177 Portugal) y donde se filtran los datos
SELECT
    *
from arm_source_209 as2
--	where arm_ab_test  is not null  -- A/B testing
    limit 1
;

|id_source|id_arm|arm_ab_test|address|aff_sub|arm_birthdate|arm_browser|arm_checkbox|arm_click_id|arm_education_campus|arm_education|arm_fin_time|arm_fire_pixel_cft|arm_fire_pixel_fbk|arm_fire_pixel_ggl|arm_fire_pixel_outbrain|arm_fire_pixel_taboola|arm_fire_pixel_twitter|arm_first_name|arm_gender|arm_height|arm_id|arm_idprovince|arm_ini_time|arm_ip|arm_is_mobile|arm_lang|arm_last_name|arm_lead|arm_option_oppositions|arm_nationality|arm_os|arm_response_server_pixel|arm_standard_education|arm_standard_nationality|arm_start_oposiciones|arm_start_job|arm_participate|arm_referrer|arm_step|arm_step_order|arm_width|cf_uvid|check_pixel_default_behavior|cp_pixel|click_phone_popup|click_popup|checkbox_114|checkbox_39ec|checkbox_13ec|coreg_adls|coreg_mm|country|created_at|deal_13|crs_4|deal_65|deal_20|email|end_cli_19|end_cli_24|funnel_id|funnel_step_id|gender_pixel|education_pixel|nationality_pixel|id|ip|module_48_q_1|module_45_q_1|module_46_q_1|module_46_q_2|module_47_q_1|module_47_q_2|module_49_q_1|module_51_q_1|module_51_q_2|module_52_q_1|module_52_q_2|module_52_q_3|module_52_q_4|module_52_q_5|module_52_q_6|module_52_q_7|module_52_q_8|module_53_q_1|module_53_q_2|module_50_q_1|module_50_q_3|module_36_q_1|module_37_q_1|module_37_q_2|module_37_q_3|module_60|module_61|module_62|module_54_q_1|module_54_q_2|module_39_q_1|module_42_q_1|module_43_q_1|module_44_q_1|module_44_q_2|module_50_q_2|module_38_q_1|max_age_pixel|min_age_pixel|module_58_q_1|module_58_q_2|module_41_q_1|module_56_q_1|module_56_q_2|module_56_q_3|module_56_q_4|module_41_q_2|module_40_q_1|module_40_q_2|module_25_q_1|module_25_q_2|module_26_q_1|module_26_q_2|module_24_q_1|module_18_q_1|module_28_q_1|module_28_q_2|module_15_q_1|module_15_q_2|module_15_q_3|module_15_q_4|module_17_q_1|module_20_q_1|module_20_q_2|module_20_q_3|module_20_q_4|module_22_q_1|module_23_q_1|module_29_q_1|module_36_i_1|module_30_q_1|module_35_q_1|module_35_q_2|module_32_q_1|module_27_q_1|module_31_q_1|module_34_q_1|module_34_q_2|module_21_q_1|module_16_q_1|module_16_q_2|module_19_q_1|module_11_q_1|module_13_q_1|module_12_q_1|module_14_q_1|module_10_q_1|module_10_q_2|module_6_q_1|module_6_q_2|module_8_q_1|module_8_q_2|module_6_q_3|module_9_q_1|module_5_1|module_4_q_1|module_7_q_1|module_7_q_2|module_2_q_1|module_2_q_2|module_2_q_3|module_2_q_2_o|module_2_q_4|module_1_q_1|module_1_q_2|module_1_q_3|module_3_q_1|module_3_q_2|name|module_59|page_id|phone|q_1|q_2|q_6|q_3|q_ref_0|q_ref_1|time_zone|q_4|q_5|question|salud_0|salud_sub_0|sp1_2|sp_1|sp_2|state|utm_campaign|utm_content|utm_medium|utm_source|utm_term|zip|city|coreg_deal_1|id_filter|last_update|insert_date|
|---------|------|-----------|-------|-------|-------------|-----------|------------|------------|--------------------|-------------|------------|------------------|------------------|------------------|-----------------------|----------------------|----------------------|--------------|----------|----------|------|--------------|------------|------|-------------|--------|-------------|--------|----------------------|---------------|------|-------------------------|----------------------|------------------------|---------------------|-------------|---------------|------------|--------|--------------|---------|-------|----------------------------|--------|-----------------|-----------|------------|-------------|-------------|----------|--------|-------|----------|-------|-----|-------|-------|-----|----------|----------|---------|--------------|------------|---------------|-----------------|--|--|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|---------|---------|---------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|-------------|------------|------------|------------|------------|------------|------------|----------|------------|------------|------------|------------|------------|------------|--------------|------------|------------|------------|------------|------------|------------|----|---------|-------|-----|---|---|---|---|-------|-------|---------|---|---|--------|-------|-----------|-----|----|----|-----|------------|-----------|----------|----------|--------|---|----|------------|---------|-----------|-----------|
|1|1510066387250||||1929/02/1|Google Chrome|Y|b2uxalnwe9zt|||15:58:9|||||||Test Competo|M|667|||15:53:7||false|es|Con Utm|test@test.com|||MacOS||||||||question||1362|null||||||||||Spain|2017-11-07 14:58:10.0|||||1510066387250@gasolineras.reembolsodirecto.club|||4847054|22665541||||217925721|217.126.178.147|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||Test Competo con UTM||17405862|910000000|fiat panda|95||50||||Y|13|prueba completa AFL||||||Madrid|10027|testUTM_C|testUTM_M|FBK|testUTM_T|28000|Madrid|N|4||2018-08-12 20:15:05.0|


/*
-- tabla VIP vendible
*
* gender: M|F|2
*
*/

select
	*
--    distinct(id_unsuscribed )
from arm_leads_209 al
    limit 1;


-- |id_lead|id_arm|id_getway|id_country|id_filter|id_city|id_state|id_unsuscribed|referer_url|full_name|first_name|last_name|gender|email|phone|address|zip_code|birth_date|ip_address|utm_source|utm_term|utm_campaign|utm_content|utm_medium|doi|created_at|inserted_at|unsuscribed_at|extra_data|id_unsubscribe_reason|kids|city|
-- |-------|------|---------|----------|---------|-------|--------|--------------|-----------|---------|----------|---------|------|-----|-----|-------|--------|----------|----------|----------|--------|------------|-----------|----------|---|----------|-----------|--------------|----------|---------------------|----|----|
-- |1|1510912675709|4847054|209|1|Barcelona|Catalonia|||Jorge Clemente Bo|Jorge|Bo|M|jorboe@hotmail.com|669664911||46722|1978/09/16|195.77.84.142|FBK||10027||||2017-11-17 10:01:14.0|2017-11-17 11:11:24.0||||1||

   
select * from arm_leads limit 1;

-- tabla Submission

select id_delivery from arm_delivery delivery where DATE(delivery.delivery_at) = CURRENT_DATE(); 

select count(*) from arm_submission_delivery asd where id_delivery IN (
select id_delivery from arm_delivery delivery where DATE(delivery.delivery_at) = CURRENT_DATE() 
);
   

  
SELECT
    -- count(*) -- count 28,486,392
    *
from arm_submissions as2 
limit 1
;

-- |id_submission|id_deal|id_deal_getway|id_lead|id_arm|id_delivery_service|id_rtraslate|response|transaction_info|response_at|delivery_date|insert_date|old_id_ref|id_process|id_product|is_used|
-- |-------------|-------|--------------|-------|------|-------------------|------------|--------|----------------|-----------|-------------|-----------|----------|----------|----------|-------|
-- |1|1|1|1|1505421971962|1|2|ko5|http://x.oferta.medicare.pt/coreg.33bf2c8147e950a9170434ba8c86991b.php?mclic=P4799356CDC113&p1=Vieira&p2=Maria&p3=mmvieira34%40hotmail.com&p4=966548975&p6=combustivel.reembolsodirecto.com&p7=2610-303&p9=1||2017-09-21 17:44:41.0|2017-09-21 17:44:41.0|1|1|||

-- tabla submission_delivery

select
    -- count(*) -- count 46,784,190
    *
from arm_submission_delivery asd
 limit 1
;
ignorar  


-- |id_submission|id_deal|id_arm|id_delivery|id_rtranslate|response|transaction_info|id_process|id_product|created_at|updated_at|
-- |-------------|-------|------|-----------|-------------|--------|----------------|----------|----------|----------|----------|

/*
-- -----------------------------
--  arm_deals
-- Dnd está el trio id_client, id_end_client, id_deal_type
-- -----------------------------
*/
describe arm_deals;
select 
	*
from arm_deals
limit 1
;

-- |id_deal|id_client|id_country|description                 |comments|id_deal_type|id_service|start_date|end_date|id_end_client|id_delivery_service|fixed_arm_origin|id_business|send_sample_leads_dpo|more_one_delivery|alert_download_csv|minum_leads|maximum_leads|deal_without_deliveries|time_download|max_size|b_enabled|contacts|steps_fields|filters_deals|query_deals|type_send_leads|estimated_approval_rate|is_invoiceable|id_user|created_at|updated_at|
-- |-------|---------|----------|-----------                 |--------|------------|----------|----------|--------|-------------|-------------------|----------------|-----------|---------------------|-----------------|------------------|-----------|-------------|-----------------------|-------------|--------|---------|--------|------------|-------------|-----------|---------------|-----------------------|--------------|-------|----------|----------|
-- |1      |1        |177       |KWO - Medicare - Layer (Old)|        |7           |1         |2017-09-17 00:00:00.0|2017-10-17 00:00:00.0|10|1|1|4|0|0|0|5|0|0|2|1|0|||{"apiStep":"query"}||filter|1.0|1||2017-12-26 00:00:00.0||



/*
 * Getways (sic)
 */
select * from arm_getways ag where b_enabled =1
limit 1
;


|id_getway|id_country|id_concept|id_business_unit|platform|url|description|b_enabled|hook|gateway_id|id_gateway_type|gateway_url_campaign|gateway_url_action|gateway_url_origin|gateway_url_supplier|webhooks_enabled|id_user|created_at|updated_at|
|---------|----------|----------|----------------|--------|---|-----------|---------|----|----------|---------------|--------------------|------------------|------------------|--------------------|----------------|-------|----------|----------|
|6008389|177|9|4|clickfunnels|moda.oportunidades-do-dia.com|6008389 - PT_Fashion_28-8-18|1|Vale de compras na Primark (30 EUR)|46|1|||||1|2|2018-08-28 07:38:19.0|2018-08-28 07:38:19.0|

select * from arm_leads_209 leads 
	inner join arm_getways gateway on gateway.id_getway = leads.id_getway 
limit 1
;

/*
 ZipCodes y municipios
 */
select
    DISTINCT state_name from arm_zipcodes_es aze
where state_name like '%cast%'
    limit 1;




select * from arm_clients limit 1;
-- |id_client|id_country_adm|id_country_invoice|client_name|vat_number|company_name|european_vat|address|zip_code|city|short_name|url_privacy_policy|b_enabled|default_csv_download_days|id_payment_term|id_group_clients|id_user|created_at|updated_at|
-- |---------|--------------|------------------|-----------|----------|------------|------------|-------|--------|----|----------|------------------|---------|-------------------------|---------------|----------------|-------|----------|----------|
-- |1|177|76|Kwanko SA|FR50440546885|Kwanko|1|60 BD DU MARECHAL JOFFRE|92340|Lisbon|KWO||0|2|2|1||||

select * from arm_end_clients aec limit 1;
-- |id_end_client|description|group|id_group|insert_date|id_country|id_user|created_at|updated_at|
-- |-------------|-----------|-----|--------|-----------|----------|-------|----------|----------|
-- |1|Adeslas (Dental)|Insurance - Health|1|2017-12-22 00:00:00.0|209||||

/**
  Clientes y grupos de Clientes
 */


select * from arm_group_end_clients agec limit 1; 

select * from arm_end_clients end_clients
                  inner join arm_group_end_clients gouped_end_clients on end_clients.id_group = gouped_end_clients.id_group_end_clients
    and end_clients.id_country = 209

-- clientes con su grupo
select
    arm_clients.client_name ,
    arm_group_clients.name_group
from arm_clients
         inner join arm_group_clients on arm_group_clients.id_group_clients  = arm_clients.id_group_clients 
;

/*
 * rm_products_filters
 */

SELECT * FROM arm_products_filters  limit 1;

-- |id|id_product                          |id_deal|step|type_send_leads|filters|id_user|created_at|updated_at|
-- |--|----------                          |-------|----|---------------|-------|-------|----------|----------|
-- |1 |e491dbfa-c5c1-11ea-8c75-0aed12cdb94c|6|final|query|query-que-yo-dani-he-quitado||2020-07-14 13:05:14.0|2020-12-07 13:35:06.0|


SELECT 
	id_product,
	filters 
FROM arm_products_filters  
where id_deal = 10
-- and  filters like '%cp-range%';
;


/*
 * arm_unsubscribe_reasons
 * */        
-- Razones por las que una persona se da de baja
-- 1) Persona quiere (ha enviado email a digital arm), 2) Digital Arm lo ha hecho manualmente , 3) lo ha pedido el cliente
select * from arm_unsubscribe_reasons aur ;
describe arm_unsubscribe_reasons;




SELECT *  from arm_translation_ids_country;


select * from arm_country ac ;


 
select * from arm_translation_ids_education;
describe arm_translation_ids_education;

select * from  arm_translation_ids_education_177;

describe arm_translation_ids_education_177;

select * from arm_view_nationality_and_education_translation;
describe arm_view_nationality_and_education_translation;

select * from arm_translation_ids_country;
select * from arm_country ac ;