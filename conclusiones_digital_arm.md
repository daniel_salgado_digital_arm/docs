# conceptos marketing

CPC: cost per click. 
CPM: pagar por impresiones, para grandes marcas que no esperan convertir
CPL: cost per lead. Siempre tienen un CPC anterior
CPA: cost per adquisition. Pagas por venta, nosotros tenemos 1 asi, el id 50

# reglas de negocion de ORO

SIEMPRE se envían primero los leads más antiguos. En query ORDER by date ASC
NUNCA se envían leads que están excluidos. Esto se traduce en arm_leads_209.unsubscribed_at IS NULL

# terminos

gateway/landing: formulario de contacto
pixel: antes era una imagen 1x1, luego fue un js, ahora es una comunicación servidor-servidor
corregistros: se añade unos STEPs (preguntas extra) a un gateway para dar más info al lead. En el código (y en ddbb) se le conoce como module
exclusiones. Listados de teléfonos se excluyen
cliente de facturación. Cliente que paga la factura
cliente final. Cliente que llama a las personas interesadas. Existen agrupaciones de clientes (Metilife, VentaskK, V3 etc...). 
deal: contrato 'triplete'. Cliente-facturable | Cliente-final | Tipo de lead. Ej: TPP_Deco_List
Client_id: id de google
Sales funnel: embudo de ventas. x ej: de 100 que entran en web, 30 rellenan en carrito, 1 compra
utm_(campaign|source): nomenclatura a la que nos adherimos de FB para identificar datos de marketing 
concurso NO es lo mismo que sorteo: concurso depende únicamente del azar, sorteo es una competición entre los participantes



# tipo de lead (tipo de producto)

1) Standard/Normales
   - Listas frías "lista": leads que llevan más de 30 días en bbdd currentdate()>=30
   - Sponsor: 
     - Corregistros: tienen STEPS/pasos/preguntas extras. El lead esta CUALIFICADO, ya que tiene más valor.
       - Lista
       - Sponsor

2) Hot:
Son de personas muy interesadas. Según se registran se envían. Lo ideal es enciarlos a los 15min.

# actores

G/F/I (G): google, facebook, instagram. Tienen huecos para poner publicidad.
Persona: navega por internet, G le ofrece publicidad dirigida, es quien da dinero para servicios (una oposición, un concurso)
DigitalARM (DA): compra en G huecos que redirigen a Gateway. COMPRA-0 (C0) xej: compra a G a 0.5 euros por cada lead enviado.
Empresa (E): que quiere clientes. Por ejemplo academia de inglés. COMPRA-1 (C1) xej: compra a DA a 1 cada lead relleno y valido
Agencia de publicidad \[optional\]. Hay E que delegan su estrategia digital a Agencias de publicidad
ClickFunnels: "especie" de Wix especializada en Sales Funnels

Hay negocio siempre y cuando C0 << C1

# proyectos

- Backoffice: mother's lamb. Es donde los digitalarmeros curran. Procesa, crones, (ya añadiré más cosas por aqui)
- Inteligence: "un curl de envío de leads". "I/O leads". Backoffice delega en este proyecto. TODO-PREG (algo mencionó Gabriel sobre API, ¿qué era?, tb puedo mirar el código)
- MicroService: pequeños reportes y (cada vez menos) envío de emails
- Tracking: Deduplicar y píxeles
- ClickFunnels: Agrupar todos los .js de los gateways

# modelo de datos

209 es España
177 es Portugal

1) llegada de los leads
Las tablas se cruzan por el id_arm

__flujos en tablas de entrada__

arm_sources_json ---(troceado por pais)---> arm_source_209 ---(validado)---> arm_leads_209

- arm_sources_json
  - tabla gigantesca 100 millones. clickFunnel nos envía 2 jsones (uno mini, otro más completo) por cada "cambio de step". Una vez unidos se segrega en ....
- arm_source_209  y arm_source_177
  - tabla segregada por país. Una vez validado se añade a ...
- arm_leads_209 (mirar más abajo en tablas VIP)


2) envío de los leads

__flujos en tablas de salida (ventas)__

delivery -> submission

mirar en sql.sql para queries interesantes

__ tablas VIP __

arm_leads_{id_pais} es de DONDE vendemos los datos
    - id_arm: identifica el lead
    - id_gateway: identifica el gateway

arm_deal (nota que esto es clienteFacturacion,clienteFinal,Tipo-Producto)
    - id_client
    - id_end_client
    - tipo_producto


submission (api) y submission_delivery (csv). Relaciona arm_leads con arm_deal. Es donde queda reflejado lo enviado a los E
    - id_arm
    - id_deal
    - id_translate (1=OK, 2=Repe, 8=se_puede_reenviar)

__manera de relacionar datos__

básicamente la columna que identifica el lead es id_arm. Aparece tanto en flujos_tablas_de_entrada como en flujos de salida



# Estrategias

Vender al cliente más caro.
No vender la misma persona a 2 clientes 


# Unidades de negocio

# Clientes más VIP

# Detalle de 1 'ciclo normal'
Esto es lo que vi con Andrés. Un ejemplo de una persona que clicaba en 1 de nuestros Gateways.

Existe un api que te dice si un teléfono es nuevo o repetido